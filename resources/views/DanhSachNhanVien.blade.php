<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jollibee-Nhân viên</title>
    <!-- Import Boostrap css, js, font awesome here -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">       
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link href="./css/style.css" rel="stylesheet">
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
    .search-result {
        border: 1px solid;
        background: aliceblue;
    }
</style>
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
        <div class="container-fluid">
            <a class="navbar-branch" href="#">
                <img src="./images/logo.png" height="50">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" 
                data-target="#navbarResponsive">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Trang Chủ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Thực Đơn</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Khuyến mãi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Tin Tức</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/nhan-vien">Nhân Viên</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Liên Hệ</a>
                    </li>
                    @if (Auth::check())
                    <li class="nav-item">
                        <p>{{ 'Xin chào ' .Auth::user()->name }}</p>
                        <a href="/dang-xuat">Đăng xuất</a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link" href="/dang-nhap">Login</a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <h4 class="text-center">Danh sách nhân viên</h4>
        @if(session()->has('message'))
        <div style="color: red">
            {{ session()->get('message') }}
        </div>
        @endif
        <a class="btn btn-outline-light" style="color: black" href="/danh-sach-nhan-vien">Tất cả nhân viên</a>
        <a class="btn btn-outline-light" style="color: black" href="/danh-sach-nhan-vien?position=nhanvienquay">Nhân viên quầy</a>
        <a class="btn btn-outline-light" style="color: black" href="/danh-sach-nhan-vien?position=nhanvienban">Nhân viên bàn</a>
        <a class="btn btn-outline-light" style="color: black" href="/danh-sach-nhan-vien?position=nhanvienbep">Nhân viên bếp</a>
        <h4 class="text-center">Tìm kiếm nhân viên</h4>
        <input type="text" id="search_employee" class="text-center">
        <ul class="search-result" style="display: none">
            <li>test</li>
        </ul>
        <table>
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên</th>
                    <th>Username</th>
                    <th>Số điện thoại</th>
                    <th>Quê Quán</th>
                    <th>Vị trí</th>
                    <th colspan="2">Hành động</th>
                </tr>
            </thead>
            <tbody>
                @php $stt = 0 @endphp
                @foreach($listNhanVien as $nhanvien)
                @php $stt++ @endphp
                <tr>
                    <td>{{ $stt }}</td>
                    <td>{{ $nhanvien->name }}</td>
                    <td>{{ $nhanvien->username }}</td>
                    <td>{{ $nhanvien->phone }}</td>
                    <td>{{ $nhanvien->home_town }}</td>
                    <td>{{ $nhanvien->position}}</td>
                    <td>
                        <a class="btn btn-outline-light" style="color: black" href="/sua-nhan-vien?id={{ $nhanvien->id }}">Sửa</a>
                        <a class="btn btn-outline-light" style="color: black" href="/xoa-nhan-vien?id={{ $nhanvien->id }}">Xoá</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
        <div class="container-fluid padding">   
            <div class="row text-center padding">
                <div class="col-12">
                    <h2>Hãy kết nối với chúng tôi</h2>
                </div>
                <div class="col-12 social padding">
                    <a href="#"><i class="fab fa-facebook"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-google-plus-g"></i></a>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="container-fluid padding">   
            <div class="row text-center">
                <div class="col-md-4">
                    <img src="./images/logo.png">
                    <hr class="light">
                    <p>Địa chỉ: Lầu 5, tòa nhà SCIC, 16 Trương Định, Phường 6, Quận 3, Tp. Hồ Chí Minh, Việt Nam</p>
                </div>
                <div class="col-md-4">              
                    <hr class="light">
                    <h5>Work</h5>
                    <hr class="light">
                    <p>Mở cửa từ 8h sáng đến 21h các ngày trong tuần.</p>
                    <p>Giao hàng tận nơi miễn phí.</p>
                </div>
                <div class="col-md-4">              
                    <hr class="light">
                    <h5>Contact</h5>
                    <hr class="light">
                    <p>Tổng đài: 1900-1533</p>
                    <p>Hộp thư góp ý: jbvnfeedback@jollibee.com.vn</p>
                </div>
                <div class="col-12">
                    <hr class="light-100">
                    <h5>&copy; Jollibe Việt Nam</h5>
                </div>
            </div>
        </div>
    </footer>
<script>
    var typingTimer;

    //on keyup, start the countdown
    $('#search_employee').on('keyup', function () {
        if ($(this).val()) {
            var $this = $(this);
            clearTimeout(typingTimer);
            typingTimer = setTimeout(function() { 
                $.ajax({
                    url: '/tim-nhan-vien',
                    type: 'get', 
                    data : {
                        name : $this.val()
                    },
                    dataType: 'json',
                    success: function(data)
                    {
                        data.data.forEach(function(item) {
                            $('.search-result').show();
                            $('.search-result').empty();
                            $('.search-result').append(`<li><a href="/sua-nhan-vien?id=`+item.id+`">`+item.name+`</a></li>`);
                        })
                    } 
                });
            }, 500);
        } else {
            $('.search-result').hide();
        }
    });

    //on keydown, clear the countdown 
    $('#search_employee').on('keydown', function () {
        clearTimeout(typingTimer);
    });
</script>
</body>
</html>