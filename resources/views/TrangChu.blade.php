<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jollibee-Trang chủ</title>
    <!-- Import Boostrap css, js, font awesome here -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">       
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
    <div class="container-fluid">
        <a class="navbar-branch" href="#">
            <img src="{{ asset('images/logo.png') }}" height="50">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" 
            data-target="#navbarResponsive">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="/">Trang Chủ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Thực Đơn</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Khuyến mãi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Tin Tức</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="/nhan-vien">Nhân Viên</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Liên Hệ</a>
                </li>
                @if (Auth::check())
                <li class="nav-item">
                    <p>{{ 'Xin chào ' .Auth::user()->name }}</p>
                    <a href="/dang-xuat">Đăng xuất</a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="/dang-nhap">Login</a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<!-- Carousel -->
<div id="slides" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
        <li data-target="#slides" data-slide-to="0" class="active"></li>
        <li data-target="#slides" data-slide-to="1"></li>      
    </ul>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="{{ asset('images/carousel5.png') }}">
            <div class="carousel-caption">
                <button type="button" class="btn btn-outline-light btn-lg">
                    Đặt Hàng
                </button>
            </div>
        </div>
        <div class="carousel-item">
            <img src="{{ asset('images/BNKM.png') }}">
            <div class="carousel-caption">
                <h1 class="display-2">Jollibee Team</h1>
                <h3>Cùng Tháng 11 Năng Động</h3>
                <button type="button" class="btn btn-outline-light btn-lg">
                    Đặt Ngay
                </button>
            </div>
        </div>
    </div>
</div>
<!-- jumbotron -->
<div class="container-fluid padding">
    <div class="row welcome text-center">
        <hr> 
        <div class="col-12">
            <h1 class="display-4">Ăn Gì Hôm Nay</h1>
        </div>
    </div>
</div>
<!-- -->
<div class="container-fluid padding">
    <div class="row padding">
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="{{ asset('images/GaGion.png') }}">
                <div class="card-body">
                    <h4 class="card-title">Gà Giòn Vui Vẻ</h4>
                    <a href="#" class="btn btn-outline-secondary">Đặt ngay</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="{{ asset('images/GaSotCay.png') }}">
                <div class="card-body">
                    <h4 class="card-title">
                        Gà Sốt Cay    
                    </h4>
                    <a href="#" class="btn btn-outline-secondary">Đặt ngay</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="{{ asset('images/MyY.png') }}">
                <div class="card-body">
                    <h4 class="card-title">Mỳ Ý</h4>
                    <a href="#" class="btn btn-outline-secondary">Đặt ngay</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- -->
<div class="container-fluid padding">
    <div class="row welcome text-center">
        <!-- Horizontal Rule -->
        <hr> 
        <div class="col-12">
            <h1 class="display-4">Dịch Vụ</h1>
        </div>
    </div>
</div>
<!-- -->
<div class="container-fluid padding">
    <div class="row padding">
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="{{ asset('images/SinhNhat.png') }}">
                <div class="card-body">
                    <h4 class="card-title">Sinh Nhật</h4>
                    <a href="#" class="btn btn-outline-secondary">Xem Thêm</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="{{ asset('images/KIDCLUBB.png') }}">
                <div class="card-body">
                    <h4 class="card-title">
                        Kid Club 
                    </h4>
                    <h4 class="card-title">
                        TeamWork 
                    </h4>
                    <a href="#" class="btn btn-outline-secondary">Xem Thêm</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="{{ asset('images/DHLLL.png') }}">
                <div class="card-body">
                    <h4 class="card-title">Đơn Hàng</h4>
                    <h4 class="card-title">Lớn Và Nhỏ</h4>
                    <a href="#" class="btn btn-outline-secondary">xem Thêm</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- -->
<hr>
<div class="container-fluid padding">
    <div class="row padding">
        <div class="col-md-12 col-lg-6">
            <h2>Jollibee,xin chào</h2>
            <p>Chúng tôi là Jollibee Việt Nam với hơn 100 cửa hàng trên khắp cả nước, chúng tôi mong muốn đem đến niềm vui ẩm thực cho mọi gia đình Việt bằng những món ăn có chất lượng tốt, hương vị tuyệt hảo, dịch vụ chu đáo với một mức giá hợp lý… Hãy đến và thưởng thức nhé!</p>
            <br>
        </div>
        <div class="col-lg-6">
            <img src="{{ asset('images/xinchaoo.png') }}" class="img-fluid">
        </div>
    </div>
    <hr class="my-4">
</div>
<div class="container-fluid padding">
    <div class="row welcome text-center">
        <!-- Horizontal Rule -->
        <div class="col-12">
            <h1 class="display-4">Tin Tức</h1>
        </div>
    </div>
</div>
<!-- -->
<div class="container-fluid padding">
    <div class="row padding">
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="{{ asset('images/TT11.png') }}">
                <div class="card-body">
                    <a href="#" class="btn btn-outline-secondary">Xem Thêm</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="{{ asset('images/TT22.png') }}">
                <div class="card-body">
                    <a href="#" class="btn btn-outline-secondary">Xem Thêm</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="{{ asset('images/TT44.png') }}">
                <div class="card-body">
                    <a href="#" class="btn btn-outline-secondary">xem Thêm</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid padding">   
    <div class="row text-center padding">
        <div class="col-12">
            <h2>Hãy kết nối với chúng tôi</h2>
        </div>
        <div class="col-12 social padding">
            <a href="#"><i class="fab fa-facebook"></i></a>
            <a href="#"><i class="fab fa-twitter"></i></a>
            <a href="#"><i class="fab fa-google-plus-g"></i></a>
            <a href="#"><i class="fab fa-instagram"></i></a>
            <a href="#"><i class="fab fa-youtube"></i></a>
        </div>
    </div>
</div>  
<footer>
    <div class="container-fluid padding">   
        <div class="row text-center">
            <div class="col-md-4">
                <img src="{{ asset('images/logo.png') }}">
                <hr class="light">
                <p>Địa chỉ: Lầu 5, tòa nhà SCIC, 16 Trương Định, Phường 6, Quận 3, Tp. Hồ Chí Minh, Việt Nam</p>
            </div>
            <div class="col-md-4">              
                <hr class="light">
                <h5>Work</h5>
                <hr class="light">
                <p>Mở cửa từ 8h sáng đến 21h các ngày trong tuần.</p>
                <p>Giao hàng tận nơi miễn phí.</p>
            </div>
            <div class="col-md-4">              
                <hr class="light">
                <h5>Contact</h5>
                <hr class="light">
                <p>Tổng đài: 1900-1533</p>
                <p>Hộp thư góp ý: jbvnfeedback@jollibee.com.vn</p>
            </div>
            <div class="col-12">
                <hr class="light-100">
                <h5>&copy; Jollibe Việt Nam</h5>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
