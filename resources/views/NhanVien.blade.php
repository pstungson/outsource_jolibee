<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jollibee-Nhân viên</title>
    <!-- Import Boostrap css, js, font awesome here -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">       
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link href="./css/style.css" rel="stylesheet">
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
    <div class="container-fluid">
        <a class="navbar-branch" href="#">
            <img src="./images/logo.png" height="50">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" 
            data-target="#navbarResponsive">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">Trang Chủ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Thực Đơn</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Khuyến mãi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Tin Tức</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="/nhan-vien">Nhân Viên</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Liên Hệ</a>
                </li>
                @if (Auth::check())
                <li class="nav-item">
                    <p>{{ 'Xin chào ' .Auth::user()->name }}</p>
                    <a href="/dang-xuat">Đăng xuất</a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="/dang-nhap">Login</a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<!-- Carousel -->
<div id="slides" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
        <li data-target="#slides" data-slide-to="0" class="active"></li>
        <li data-target="#slides" data-slide-to="1"></li>
        <li data-target="#slides" data-slide-to="2"></li>       
    </ul>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="./images/carousel0.png">
            <div class="carousel-caption">
                <h1 class="display-2">Jollibee Team</h1>
                <h3>Cùng Tháng 11 Năng Động</h3>
                <button type="button" class="btn btn-outline-light btn-lg">
                    Tuyển dụng tháng 12
                </button>
            </div>
        </div>
        <div class="carousel-item">
            <img src="./images/carousel1.png">
            <div class="carousel-caption">
                <h1 class="display-2">Jollibee Team</h1>
                <h3>Cùng Tháng 11 Năng Động</h3>
                <button type="button" class="btn btn-outline-light btn-lg">
                    Tuyển dụng tháng 12
                </button>
            </div>
        </div>
        <div class="carousel-item">
            <img src="./images/carousel2.png">
            <div class="carousel-caption">
                <h1 class="display-2">Jollibee Team</h1>
                <h3>Cùng Tháng 11 Năng Động</h3>
                <button type="button" class="btn btn-outline-light btn-lg">
                    Tuyển dụng tháng 12
                </button>
            </div>
        </div>
    </div>
</div>
<!-- jumbotron -->
<div class="container-fluid padding">
    <div class="row welcome text-center">
        <!-- Horizontal Rule -->
        <hr> 
        <div class="col-12">
            <h1 class="display-4">Management List</h1>
        </div>
    </div>
</div>
<!-- -->
<div class="container-fluid padding">
    <div class="row text-center padding">
        <div class="col-xs-12 col-sm-6 col-md-4 ">
            <img src="./images/ql1.png" height="400">
            <h3>Quản Lý</h3>
            <p>Nguyễn Tuấn Hiển</p>                    
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <img src="./images/ql2.png" height="400">         
            <h3>Quản Lý</h3>
            <p>Bùi Thị Diệp</p>
        </div>
        <div class="col-sm-12 col-md-4">
            <img src="./images/ql3.png" height="400">         
            <h3>Quản Lý</h3>
            <p>Lục Minh Giang</p>
        </div>
    </div>  
    <hr class="my-4">   
</div>
<!-- -->
<div class="container-fluid padding">
    <div class="row padding">
        <div class="col-md-12 col-lg-6">
            <h2>Về Jollibee.</h2>
            <p>Đạt được cột mốc cửa hàng thứ 100, Jollibee - chuỗi nhà hàng thức ăn nhanh lớn thứ 3 tại Việt Nam – càng củng cố cam kết lan tỏa niềm vui bất tận đến các gia đình Việt Nam.</p>
            <p>Ông Lâm Hồng Nguyên, Tổng Giám đốc Jollibee Việt Nam, cho biết: “Jollibee luôn cố gắng lan tỏa niềm vui đến mợi người ở khắp mọi nơi. Với hệ thống 100 cửa hàng trên cả nước, chúng tôi vô cùng phấn khởi khi có thể đưa Jollibee - thương hiệu của niềm vui và tiếng cười – đến gần hơn với các gia đình Việt từ Bắc chí Nam”.</p>
            <br>
        </div>
        <div class="col-lg-6">
            <img src="./images/GiamDoc11.png" height="500">
        </div>
    </div>
</div>
<!-- -->
<div class="container-fluid padding">
    <div class="row welcome text-center">
        <div class="col-12">
            <h1 class="display-4">Jollibee Team</h1>
        </div>
    </div>
</div>
<!-- -->
<div class="container-fluid padding">
    <div class="row padding">
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="./images/DSNV.png">
                <div class="card-body">
                    <h4 class="card-title">Danh sách nhân viên</h4>
                    <a href="/danh-sach-nhan-vien" class="btn btn-outline-secondary">Xem thêm</a>
                    <a href="/them-nhan-vien" class="btn btn-outline-secondary float-right">Thêm nhân viên</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="./images/LICH.png">
                <div class="card-body">
                    <h4 class="card-title">
                        Lịch làm    
                    </h4>
                    @if (Auth::check())
                    <a href="/lich-lam" class="btn btn-outline-secondary">Xem thêm</a>
                    @else
                    <a href="/dang-nhap" class="btn btn-outline-secondary">Xem thêm</a>
                    @endif
                    @if (Auth::check())
                    <a href="/dang-ky-lich" class="btn btn-outline-secondary float-right">Đăng ký lịch</a>
                    @else
                    <a href="/dang-nhap" class="btn btn-outline-secondary float-right">Đăng ký lịch</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="./images/THIDUA.png">
                <div class="card-body">
                    <h4 class="card-title">Bảng thi đua</h4>
                    <a href="/bang-thi-dua?position=nhanvienquay" class="btn btn-outline-secondary">Xem thêm</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="./images/TUYENDUNG.png">
                <div class="card-body">
                    <h4 class="card-title">Tuyển dụng</h4>
                    <a href="#" class="btn btn-outline-secondary">Xem thêm</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="./images/KEHOACH.png">
                <div class="card-body">
                    <h4 class="card-title">Kế hoạch tháng 11</h4>
                    <a href="#" class="btn btn-outline-secondary">Xem thêm</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="./images/more1111.png">
                <div class="card-body">
                    <h4 class="card-title">
                        Về khuyễn mãi
                    </h4>
                    <a href="#" class="btn btn-outline-secondary">Xem thêm</a>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="container-fluid padding">
    <div class="row padding">
        <div class="col-md-12 col-lg-6">
            <h2>Message</h2>
            <p>“Sự cam kết của mỗi cá nhân trong một nỗ lực nhóm chính là điều tạo nên cách làm việc của một đội ngũ, một công ty, một xã hội và một nền văn minh” - Vince Lombardi</p>
            <p>“Sức mạnh của nhóm là từng thành viên. Sức mạnh của mỗi thành viên chính là nhóm.” - Phil Jackson</p>
            <br>
        </div>
        <div class="col-lg-6">
            <img src="./images/NVJB.png" class="img-fluid">
        </div>
    </div>
    <hr class="my-4">
</div>
<div class="container-fluid padding">   
    <div class="row text-center padding">
        <div class="col-12">
            <h2>Hãy kết nối với chúng tôi</h2>
        </div>
        <div class="col-12 social padding">
            <a href="#"><i class="fab fa-facebook"></i></a>
            <a href="#"><i class="fab fa-twitter"></i></a>
            <a href="#"><i class="fab fa-google-plus-g"></i></a>
            <a href="#"><i class="fab fa-instagram"></i></a>
            <a href="#"><i class="fab fa-youtube"></i></a>
        </div>
    </div>
</div>  
<footer>
    <div class="container-fluid padding">   
        <div class="row text-center">
            <div class="col-md-4">
                <img src="./images/logo.png">
                <hr class="light">
                <p>Địa chỉ: Lầu 5, tòa nhà SCIC, 16 Trương Định, Phường 6, Quận 3, Tp. Hồ Chí Minh, Việt Nam</p>
            </div>
            <div class="col-md-4">              
                <hr class="light">
                <h5>Work</h5>
                <hr class="light">
                <p>Mở cửa từ 8h sáng đến 21h các ngày trong tuần.</p>
                <p>Giao hàng tận nơi miễn phí.</p>
            </div>
            <div class="col-md-4">              
                <hr class="light">
                <h5>Contact</h5>
                <hr class="light">
                <p>Tổng đài: 1900-1533</p>
                <p>Hộp thư góp ý: jbvnfeedback@jollibee.com.vn</p>
            </div>
            <div class="col-12">
                <hr class="light-100">
                <h5>&copy; Jollibe Việt Nam</h5>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
