<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\NhanVien;
use App\Models\Calendar;
use Illuminate\Support\Facades\Auth;

class CalendarController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function create()
    {
        return view('DangKyLich');
    }

    public function store()
    {
        $data = request()->all();
        Calendar::create([
            'nhanvien_id' => Auth::user()->id,
            'day' => $data['day'],
            'shift' => $data['shift'],
        ]);

        return redirect()->back()->with('message', 'Đăng ký lịch thành công');
    }

    public function list()
    {
        $calendars = Calendar::join('nhanvien', 'calendar.nhanvien_id', '=', 'nhanvien.id')
            ->where('nhanvien.id', Auth::user()->id)
            ->get();

        return view('LichLam', compact('calendars'));
    }
}
