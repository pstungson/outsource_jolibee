<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $data = [
            'username' => $request->username,
            'password' => $request->password,
        ];

        if (Auth::attempt($data)) {
            return redirect('/');
        } else {
            return redirect()->back()->with('message', 'Sai thông tin đăng nhập');
        }
    }

    public function logout() {
        auth()->logout();
        return redirect('/');
    }
}