<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\NhanVien;
use Illuminate\Support\Facades\Auth;

class NhanVienController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function create()
    {
        return view('ThemNhanVien');
    }

    public function store()
    {
        $data = request()->all();
        NhanVien::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'phone' => $data['phone'],
            'home_town' => $data['home_town'],
            'position' => $data['position'],
        ]);
        return redirect()->back()->with('message', 'Thêm nhân viên thành công');
    }

    public function list()
    {
        if (request()->position == 'nhanvienquay') {
            $listNhanVien = NhanVien::where('position', 'Nhân viên quầy')->get();
        }
        else if (request()->position == 'nhanvienban') {
            $listNhanVien = NhanVien::where('position', 'Nhân viên bàn')->get();
        }
        else if (request()->position == 'nhanvienbep') {
            $listNhanVien = NhanVien::where('position', 'Nhân viên bếp')->get();
        } else {
            $listNhanVien = NhanVien::get();
        }
        return view('DanhSachNhanVien', compact('listNhanVien'));
    }

    public function edit()
    {
        $nhanvien = NhanVien::where('id', request()->id)->first();
        return view('SuaNhanVien', compact('nhanvien'));
    }

    public function update()
    {
        $data = request()->except('_token');
        NhanVien::where('id', request()->id)->update($data);
        return redirect()->back()->with('message', 'Sửa nhân viên thành công');
    }

    public function delete()
    {
        NhanVien::where('id', request()->id)->delete();
        return redirect()->back()->with('message', 'Xoá nhân viên thành công');
    }

    public function charts()
    {
        if (request()->position == 'nhanvienquay') {
            $listNhanVien = NhanVien::where('position', 'Nhân viên quầy')->orderBy('sales', 'DESC')->get();
        }
        else if (request()->position == 'nhanvienban') {
            $listNhanVien = NhanVien::where('position', 'Nhân viên bàn')->orderBy('sales', 'DESC')->get();
        }
        else if (request()->position == 'nhanvienbep') {
            $listNhanVien = NhanVien::where('position', 'Nhân viên bếp')->orderBy('sales', 'DESC')->get();
        }
        return view('BangThiDua', compact('listNhanVien'));
    }

    public function editSales()
    {
        return view('SuaDoanhSo');
    }

    public function postEditSales()
    {
        NhanVien::where('id', request()->id)->update(['sales' => request()->sales]);
        return redirect('/bang-thi-dua?position=nhanvienquay')->with('message', 'Cập nhật doanh số thành công');
    }

    public function searchEmployees()
    {
        $nhanvien = NhanVien::where('name', 'like', '%'.request()->name.'%')->get();
        return response()->json(['data' => $nhanvien], 200);
    }
}
