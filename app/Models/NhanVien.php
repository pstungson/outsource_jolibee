<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class NhanVien extends Authenticatable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nhanvien';

    protected $fillable = [
        'name', 'phone', 'home_town', 'position', 'username', 'password', 'sales'
    ];

    protected $hidden = [
        'remember_token',
    ];
}