<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('TrangChu');
});

Route::get('/nhan-vien', function () {
    return view('NhanVien');
});

Route::get('/dang-nhap', function () {
    return view('DangNhap');
});

Route::get('/them-nhan-vien', 'NhanVienController@create');
Route::post('/them-nhan-vien', 'NhanVienController@store');
Route::get('/danh-sach-nhan-vien', 'NhanVienController@list');
Route::get('/sua-nhan-vien', 'NhanVienController@edit');
Route::post('/cap-nhat-nhan-vien', 'NhanVienController@update');
Route::get('/xoa-nhan-vien', 'NhanVienController@delete');
Route::post('/dang-nhap', 'LoginController@login');
Route::get('/dang-xuat', 'LoginController@logout');
Route::get('/dang-ky-lich', 'CalendarController@create');
Route::post('/dang-ky-lich', 'CalendarController@store');
Route::get('/lich-lam', 'CalendarController@list');
Route::get('/bang-thi-dua', 'NhanVienController@charts');
Route::get('/sua-doanh-so', 'NhanVienController@editSales');
Route::post('/sua-doanh-so', 'NhanVienController@postEditSales');
Route::get('/tim-nhan-vien', 'NhanVienController@searchEmployees');
